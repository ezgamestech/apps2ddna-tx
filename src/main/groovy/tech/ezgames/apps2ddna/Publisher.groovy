package tech.ezgames.apps2ddna

import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.apache.http.entity.ContentType
import org.apache.log4j.Logger

/**
 * Created by idan on 28 Jul 2016.
 */
class Publisher {

    final static Logger log = Logger.getLogger(Publisher)

    final Configuration config
    final Object payload

    Publisher(Configuration config, Object payload) {
        this.config = config
        this.payload = payload
    }


    void publish() {
        def url = config.ddnaFullCollectUrl
        RESTClient restClient = new RESTClient(url, ContentType.APPLICATION_JSON)

        log.info("Posting to ${url} this payload: $payload")
        try {
            HttpResponseDecorator response = restClient.post(body: payload)

            log.info("Got response $response.status after posting $payload")
        } catch (Exception e) {
            log.error("Failed to post to $url", e)
        }
    }
}
