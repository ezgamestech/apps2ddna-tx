package tech.ezgames.apps2ddna

import com.amazonaws.services.sqs.AmazonSQSAsyncClient
import groovy.json.JsonOutput

/**
 * Created by idan on 11 Aug 2016.
 */
class PublishToAuditQueue {

    AmazonSQSAsyncClient sqsClient = new AmazonSQSAsyncClient()

    String publish(String sqsUrl, Map body) {
        String message = JsonOutput.toJson([
                type   : 'afevent',
                version: 1,
                body   : body
        ])
        sqsClient.sendMessageAsync(sqsUrl, message)
        return message
    }

}
