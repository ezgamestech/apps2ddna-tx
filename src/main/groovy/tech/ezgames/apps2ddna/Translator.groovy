package tech.ezgames.apps2ddna;

import org.springframework.stereotype.Service

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter;

/**
 * Created by idan on 28 Jul 2016.
 */
@Service
class Translator {


    static final String NON_ORGANIC = 'NON-ORGANIC'
    static final String ORGANIC = 'ORGANIC'
    private static final DateTimeFormatter AF_DATETIME_FORMATTER = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss.SSSZ')
    private static final DateTimeFormatter DDNA_DATETIME_FORMATTER = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss.SSS Z')

    Map translate(Map appsFlyerData) {
        Map ddnaEvent = [
                eventName  : 'appsFlyerAttribution',
                eventUUID  : UUID.randomUUID().toString(),
                userID     : appsFlyerData.customer_user_id,
                eventParams: [
                        acquisitionChannel: getAcquisitionChannel(appsFlyerData),
                        afAttrAdgroupID   : appsFlyerData.fb_adgroup_id,
                        afAttrAdgroupName : appsFlyerData.fb_adgroup_name,
                        afAttrAdID        : appsFlyerData.af_ad_id,
                        afAttrAdsetID     : appsFlyerData.fb_adset_id ? appsFlyerData.fb_adset_id : appsFlyerData.af_adset_id,
                        afAttrAdsetName   : appsFlyerData.fb_adset_name ? appsFlyerData.fb_adset_name : appsFlyerData.af_adset,
                        afAttrAgency      : appsFlyerData.agency,
                        afAttrCampaign    : getCampagin(appsFlyerData),
                        afAttrCampaignID  : appsFlyerData.fb_campaign_id,
                        afAttrClickID     : null,
                        afAttrClickTime   : getClickTime(appsFlyerData),
                        afAttrInstallTime : getInstallTime(appsFlyerData),
                        afAttrIsFacebook  : (appsFlyerData.fb_adgroup_id != null),
                        afAttrMediaSource : appsFlyerData.media_source,
                        afAttrMessage     : appsFlyerData.af_keywords,
                        afAttrSiteID      : appsFlyerData.af_siteid,
                        afAttrStatus      : getStatus(appsFlyerData),
                        afAttrSub1        : appsFlyerData.af_sub1,
                        afAttrSub2        : appsFlyerData.af_sub2,
                        afAttrSub3        : appsFlyerData.af_sub3,
                        afAttrSub4        : appsFlyerData.af_sub4,
                        afAttrSub5        : appsFlyerData.af_sub5,
                        platform          : appsFlyerData?.platform?.toUpperCase(),
                        sdkVersion        : appsFlyerData.sdk_version,
                ]
        ]


        ddnaEvent.eventParams = ddnaEvent.eventParams.findAll { it.value != null } //filter out null valued entries
        return ddnaEvent
    }

    private static String reformatTime(String t) {
        ZonedDateTime zdt = ZonedDateTime.parse(t, AF_DATETIME_FORMATTER)
        DDNA_DATETIME_FORMATTER.format(zdt)
    }

    private static String getInstallTime(Map appsFlyerData) {
        if (appsFlyerData.install_time_selected_timezone)
            return reformatTime(appsFlyerData.install_time_selected_timezone as String)
        return appsFlyerData.install_time
    }

    private static String getClickTime(Map appsFlyerData) {
        if (appsFlyerData.click_time_selected_timezone)
            return reformatTime(appsFlyerData.click_time_selected_timezone as String)
        return appsFlyerData.click_time
    }

    private static getCampagin(Map appsFlyerData) {
        appsFlyerData.fb_campaign_name ? appsFlyerData.fb_campaign_name : appsFlyerData.campaign
    }

    private static getAcquisitionChannel(Map appsFlyerData) {
        StringBuilder result = new StringBuilder()
        if (appsFlyerData.media_source) {
            result << appsFlyerData.media_source
        }
        def campaign = getCampagin(appsFlyerData)
        if (campaign) {
            if (result.size() > 0)
                result << ':'
            result << campaign
        }
        return result.toString()
    }

    private static def getStatus(Map body) {
        //Based on mail from ziv @ appsflyer - if the attribution_type field exists, use that as basis
        if (body.attribution_type) {
            if (body.attribution_type == 'regular')
                return NON_ORGANIC
            return ORGANIC
        }

        //else, try this...
        if (body.af_status)
            return body.af_status.toUpperCase()
        if (body.keySet().intersect(['af_ad', 'af_ad_id', 'af_channel', 'af_c_id', 'af_adset', 'af_adset_id', 'af_ad_type']))
            return NON_ORGANIC
        return ORGANIC
    }
}
