package tech.ezgames.apps2ddna

import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.apache.http.entity.ContentType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
/**
 * Receives pushes from AppsFlyer. Should push to DeltaDNA
 *
 * Created by idan on 27 Jul 2016.
 */
@RestController
class Controller {

    private static final Logger log = LoggerFactory.getLogger(Controller)

    @Autowired
    Translator translator

    @Autowired
    Configuration configuration

    @RequestMapping (path = '/transmit', method = RequestMethod.POST)
    public ResponseEntity<Map> transmit(@RequestBody Map afEventMap) {

        log.info("Received afEventMap=$afEventMap")

        Map ddnaEvent = translator.translate(afEventMap)

        log.info("Constructed ddna event: $ddnaEvent")

        publish(ddnaEvent)

        return ResponseEntity.ok().body(ddnaEvent)
    }

    void publish(Object payload) {
        def url = configuration.ddnaFullCollectUrl
        RESTClient restClient = new RESTClient(url, ContentType.APPLICATION_JSON)

        log.info("Posting to ${url} this payload: $payload")
        try {
            long t0 = System.currentTimeMillis()
            HttpResponseDecorator response = restClient.post(body: payload)

            long dt = System.currentTimeMillis() - t0
            log.info("Got response $response.status after $dt milliseconds")

        } catch (Exception e) {
            log.error("Failed to post to $url", e)
        }
    }
}
