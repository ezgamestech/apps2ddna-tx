package tech.ezgames.apps2ddna

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

/**
 * Created by idan on 28 Jul 2016.
 */
@Component
class Configuration {


    @Value ('${ez.ddna.collect.url:#{null}}')
    String ddnaCollectUrl

    @Value ('${ez.ddna.key:#{null}}')
    String ddnaKey

    String getDdnaFullCollectUrl() {
        return "$ddnaCollectUrl/$ddnaKey"
    }


}
