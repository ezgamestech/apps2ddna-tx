package tech.ezgames.apps2ddna

import spock.lang.Specification
/**
 * Created by idan on 15 Aug 2016.
 */
class TranslatorSpec extends Specification {

    def "translator works properly"() {
        given:
        def afEventMap = [
                event_type: 'event_type',
                attribution_type: 'attribution_type',
                click_time: '2016-08-10 12:27:10.174',
                download_time: '2016-08-10 12:27:10.174',
                install_time: '2016-08-10 12:27:10.174',
                media_source: 'media_source',
                agency: 'agency',
                af_channel: 'af_channel',
                af_keywords: 'af_keywords',
                campaign: 'campaign',
                af_c_id: 'af_c_id',
                af_adset: 'af_adset',
                af_adset_id: 'af_adset_id',
                af_ad: 'af_ad',
                af_ad_id: 'af_ad_id',
                fb_campaign_name: 'fb_campaign_name',
                fb_campaign_id: 'fb_campaign_id',
                fb_adset_name: 'fb_adset_name',
                fb_adset_id: 'fb_adset_id',
                fb_adgroup_name: 'fb_adgroup_name',
                fb_adgroup_id: 'fb_adgroup_id',
                af_ad_type: 'af_ad_type',
                af_siteid: 'af_siteid',
                af_sub1: 'af_sub1',
                af_sub2: 'af_sub2',
                af_sub3: 'af_sub3',
                af_sub4: 'af_sub4',
                af_sub5: 'af_sub5',
                http_referrer: 'http_referrer',
                click_url: 'click_url',
                af_cost_model: 'af_cost_model',
                af_cost_value: 'af_cost_value',
                af_cost_currency: 'af_cost_currency',
                cost_per_install: 'cost_per_install',
                is_retargeting: 'is_retargeting',
                re_targeting_conversion_type: 're_targeting_conversion_type',
                country_code: 'country_code',
                city: 'city',
                ip: 'ip',
                wifi: false,
                mac: 'mac',
                operator: 'operator',
                carrier: 'carrier',
                language: 'language',
                appsflyer_device_id: 'appsflyer_device_id',
                advertising_id: 'advertising_id',
                android_id: 'android_id',
                customer_user_id: 'customer_user_id',
                IMEI: 'IMEI',
                platform: 'platform',
                device_brand: 'device_brand',
                device_model: 'device_model',
                os_version: 'os_version',
                app_version: 'app_version',
                sdk_version: 'sdk_version',
                app_id: 'app_id',
                app_name: 'app_name',
                event_time: '2016-08-10 12:27:10.174',
                event_name: 'event_name',
                event_value: 'event_value',
                currency: 'currency',
                validated: 'validated',
                download_time_selected_timezone: '2016-08-10 12:27:10.174+0000',
                click_time_selected_timezone: '2016-08-10 12:27:10.174+0100',
                install_time_selected_timezone: '2016-08-10 12:27:10.174-0100',
                event_time_selected_timezone: '2016-08-10 12:27:10.174-1000',
                selected_currency: 'selected_currency',
                revenue_in_selected_currency: 'revenue_in_selected_currency',
                cost_in_selected_currency: 'cost_in_selected_currency'
        ]

        Translator translator = new Translator()

        when:
        def ddnaEventMap = translator.translate(afEventMap)

        def uuid = ddnaEventMap.eventUUID

        then:
        ddnaEventMap == [
                eventName:'appsFlyerAttribution',
                eventUUID:uuid, //generated randomly
                userID:'customer_user_id',
                eventParams:[
                        acquisitionChannel:'media_source:fb_campaign_name',
                        afAttrAdgroupID:'fb_adgroup_id',
                        afAttrAdgroupName:'fb_adgroup_name',
                        afAttrAdID:'af_ad_id', afAttrAdsetID:'fb_adset_id', afAttrAdsetName:'fb_adset_name', afAttrAgency:'agency', afAttrCampaign:'fb_campaign_name',
                        afAttrCampaignID:'fb_campaign_id', afAttrClickTime:'2016-08-10 12:27:10.174 +0100',
                        afAttrInstallTime:'2016-08-10 12:27:10.174 -0100',
                        afAttrIsFacebook:true,
                        afAttrMediaSource:'media_source',
                        afAttrMessage:'af_keywords',
                        afAttrSiteID:'af_siteid',
                        afAttrStatus:'ORGANIC',
                        afAttrSub1:'af_sub1',
                        afAttrSub2:'af_sub2',
                        afAttrSub3:'af_sub3',
                        afAttrSub4:'af_sub4',
                        afAttrSub5:'af_sub5',
                        platform:'PLATFORM',
                        sdkVersion:'sdk_version']
        ]
    }
}
